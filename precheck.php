<?php

/**
 * @module          Signup
 * @author          cms-lab
 * @copyright      	2014-2025 CMS-LAB
 * @link            https://cms-lab.com
 * @license         GNU GPL https://www.gnu.org/licenses/gpl-3.0.en.html
 * @license_terms   none
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

// Checking Requirements

$PRECHECK['LEPTON_VERSION']        = array(
    'LEPTON_VERSION' => '7.3.0',
    'OPERATOR' => '>='
);

$PRECHECK['ADDONS']      = array(
    'lib_phpmailer' => array(
        'VERSION' => '6.0',
        'OPERATOR' => '>='
    ),
    'lib_fomantic' => array(
        'VERSION' => '2.9',
        'OPERATOR' => '>='
    )	
);
