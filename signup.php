<?php

/**
 * @module          Signup
 * @author          cms-lab
 * @copyright      	2014-2025 CMS-LAB
 * @link            https://cms-lab.com
 * @license         GNU GPL https://www.gnu.org/licenses/gpl-3.0.en.html
 * @license_terms   none
 *
 */


// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file
 
// Make sure the login is enabled
if(!FRONTEND_LOGIN)
{
header('Location: '.LEPTON_URL.'/index.php');
exit();
} 

// form faked? Check the honeypot-fields.
if(ENABLED_ASP && isset($_POST['username']) && ( 
	(!isset($_POST['submitted_when']) || !isset($_SESSION['submitted_when'])) || 
	($_POST['submitted_when'] != $_SESSION['submitted_when']) ||
	(!isset($_POST['email-address']) || $_POST['email-address']) ||
	(!isset($_POST['name']) || $_POST['name']) ||
	(!isset($_POST['full_name']) || $_POST['full_name'])
)) 
{
		header('Location: '.LEPTON_URL.'/index.php');
		die ();
}

// get language marker
$MOD_SIGNUP = signup::getInstance()->language;

// use the keepout table, starting with L*7
if(file_exists(LEPTON_PATH.'/modules/signup/keepout_custom.php'))
{
	LEPTON_handle::include_files ('/modules/signup/keepout_custom.php');	
}
else
{
	LEPTON_handle::include_files ('/modules/signup/keepout.php');
}


// check $_POST
if(!isset($_POST['email']) || !is_numeric($_POST['submitted_when'])) 
{
	header("Location: ".$_SERVER['HTTP_REFERER']." ");
	exit();
} 
else 
{
	$user = $_POST['email'];
	if (!filter_var($user, FILTER_VALIDATE_EMAIL)) 
	{
		header('Location: '.LEPTON_URL.'/index.php');
		exit();
	}
	// part 2 ... invalid chars?
	if( false === LEPTON_handle::checkEmailChars( $user ) )
	{
		header('Location: '.LEPTON_URL.'/index.php');
		exit();	
	}		
}

if(!isset($_POST['display_name']) ) 
{
	header("Location: ".$_SERVER['HTTP_REFERER']." ");
	exit();
} 
else 
{
	$user_display = strip_tags($_POST['display_name']);
	// check invalid chars?
	if( false === LEPTON_handle::checkUsernameChars( $user_display ) )
	{
		header('Location: '.LEPTON_URL.'/index.php');
		exit();	
	}			
}

// check captcha
if(ENABLED_CAPTCHA) 
{		
	if(isset($_POST['captcha']))
	{
		// Check for a mismatch
		if( $_POST['captcha'] != $_SESSION['captcha'.$_SESSION['captcha_id']]) 
		{
			$_SESSION["signup_error"] = $MOD_SIGNUP['wrong_captcha'].'[5]';
			header("Location: ".$_SERVER['HTTP_REFERER']." ");
			return 0;
		}
	} 
	else 
	{
		$_SESSION["signup_error"] = $MOD_SIGNUP['wrong_captcha'].'[10]';
	}
}
// unset Captcha
if(isset($_SESSION['captcha'.$_SESSION['captcha_id']])) 
{
	unset($_SESSION['captcha'.$_SESSION['captcha_id']]); 
}


//Get group infos from standard signup group
$signup_group =[];
$database->execute_query(
	"SELECT * FROM ".TABLE_PREFIX."groups WHERE group_id = ".FRONTEND_SIGNUP,
	true,
	$signup_group, 
	false
);

//Get all users and check double for multiple use
$existing_users = [];
$database->execute_query(
	"SELECT * FROM ".TABLE_PREFIX."users",
	true,
	$existing_users, 
	true
);

$double_mail = false;
foreach ($existing_users as &$ref) 
{
	if($ref['email'] == $user)
	{
		$double_mail = true;
		break;
	}
}
unset ($ref);	

$double_username = false;
foreach ($existing_users as &$ref)
{
	if($ref['username'] == $user)
	{
		$double_username = true;
		break;
	}
}
unset($ref);

// create timestamp for multiple use
$timestamp = date('Y-m-d H:i:s',time());
$unix = time();

// create hash for multiple use
$hash = md5(date('Y-m-d H:i:s',time()));

// create confirmation link
$confirm_signup = $_SERVER['HTTP_REFERER'].'?hash='.$hash;


// prevent double signup email
if ($double_mail === true)
{
	$_SESSION["signup_error"] = $MOD_SIGNUP['already_signup'];
	header("Location: ".$_SERVER['HTTP_REFERER']." ");
	die();
}

// prevent double signup username
if ($double_username === true)
{
	$_SESSION["signup_error"] = $MESSAGE['USERS_USERNAME_TAKEN'];
	header("Location: ".$_SERVER['HTTP_REFERER']." ");
	die();
}

//send mail	
//Create a new PHPMailer instance
$mail = LEPTON_mailer::getInstance();
$mail->CharSet = DEFAULT_CHARSET;	
//Set who the message is to be sent from
$mail->setFrom(SERVER_EMAIL,MAILER_DEFAULT_SENDERNAME);
//Set an alternative reply-to address
$mail->addReplyTo(SERVER_EMAIL,MAILER_DEFAULT_SENDERNAME);
//Set who the message is to be sent to
$mail->addAddress($_POST['email'], $_POST['display_name']);
//Send bcc to admin
//$mail->addBCC(SERVER_EMAIL,MAILER_DEFAULT_SENDERNAME);
//Set the subject line
$mail->Subject = $MOD_SIGNUP['signup_subject'];
//Switch to TEXT messages
$mail->IsHTML(true);
$mail->Body = sprintf($MOD_SIGNUP['signup_text'],$confirm_signup);	

//send the message, check for errors
if (!$mail->send()) 
{
		$_SESSION["signup_error"] = "Mailer Error: " . $mail->ErrorInfo;
		header("Location: ".$_SERVER['HTTP_REFERER']." ");
		die();
} 
else 
{
	
	if(!isset($_POST['contact_type']))
	{
		$_POST['contact_type'] = $_POST['type'];
	}
	
	//save into database
	$fields = array(
//	'group_id'	=>	FRONTEND_SIGNUP,
	'groups_id'	=>	FRONTEND_SIGNUP,
	'active'	=>	0,	
	'statusflags'	=>	10,
	'username'	=>	$user,		
	'password'	=>	$unix,
	'display_name'	=>	$user_display,
	'email'		=>	$user,
	'timezone_string'	=> DEFAULT_TIMEZONE_STRING,
	'language'	=> DEFAULT_LANGUAGE,
	'contact_type'	=> $_POST['contact_type'],
	'registered'	=> $timestamp,			
	'hash'	=> $hash,
	'unix_time'	=> $unix
		
	);
	$database->build_and_execute(
		'INSERT', 
		TABLE_PREFIX.'users', 
		$fields
	);
		
	$_SESSION["signup_message"] = $MOD_SIGNUP['signup_info'];
	header("Location: ".$_SERVER['HTTP_REFERER']." ");
}
