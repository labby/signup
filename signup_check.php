<?php

/**
 * @module          Signup
 * @author          cms-lab
 * @copyright      	2014-2025 CMS-LAB
 * @link            https://cms-lab.com
 * @license         GNU GPL https://www.gnu.org/licenses/gpl-3.0.en.html
 * @license_terms   none
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

$database = LEPTON_database::getInstance();
global $err_msg, $MESSAGE;

// get language
$oSU = signup::getInstance();
$MOD_SIGNUP = $oSU->language;

// create timestamp for multiple use
$timestamp = date('Y-m-d H:i:s',time());
$unix = time();

// create a random password
LEPTON_handle::register('random_string');
$generate_pass = random_string(10,'pass');
$password = password_hash($generate_pass, PASSWORD_DEFAULT );

// create confirmation link
$enter_pw_link = LEPTON_URL.'/account/new_password.php?hash='.$unix.'&signup=1';

// delete users without confirmation if signup is not confirmed within 48 hours = 172800 seconds while signup page is called 
$database->execute_query("DELETE FROM `".TABLE_PREFIX."users`WHERE '".$unix."' > `unix_time`+172800 AND `statusflags` = 10 ");	

// email verification via hash
if (isset($_GET['hash']) ) 
{
	$check_hash = $_GET['hash'];
	// invalid chars?
	if( false === LEPTON_handle::checkHexChars( $check_hash ) )
	{
		header('Location: '.LEPTON_URL.'/index.php');
		exit();	
	}	

	$is_user = [];
	$database->execute_query(
		"SELECT * FROM ".TABLE_PREFIX."users WHERE hash = '".$check_hash."' ",
		true,
		$is_user,
		false
	);

	// no hash in database
	if (empty($is_user))
	{
		$is_user['hash'] = -1;
		$is_user['statusflags'] = -1;
		$is_user['active'] = -1;		
	}

	// prevent double verification 
	if( $is_user['active'] == 1 && $is_user['statusflags'] == 16) 
	{
		$_SESSION["signup_error"] = $MOD_SIGNUP['already_verfied'];
		header("Location: ".LEPTON_URL."/account/signup.php ");
		die();
		
	}

	//	update new user in database
	if( (!empty($is_user)) && ($is_user['statusflags'] == 10)) 
	{	
		//send mail	
		//Create a new PHPMailer instance
		$mail = LEPTON_mailer::getInstance();
		$mail->CharSet = DEFAULT_CHARSET;	
		//Set who the message is to be sent from
		$mail->setFrom(SERVER_EMAIL,MAILER_DEFAULT_SENDERNAME);
		//Set an alternative reply-to address
		$mail->addReplyTo(SERVER_EMAIL,MAILER_DEFAULT_SENDERNAME);
		//Set who the message is to be sent to
		$mail->addAddress($is_user['email'], $is_user['display_name']);
		//Send bcc to admin

		//Set the subject line
		$mail->Subject = $MOD_SIGNUP['signup_subject'];
		//Switch to TEXT messages
		$mail->IsHTML(true);
		$mail->Body = sprintf($MOD_SIGNUP['confirm_text'],$enter_pw_link);	

		//send the message, check for errors
		if (!$mail->send()) 
		{
				$_SESSION["signup_error"] = "Mailer Error: ".$mail->ErrorInfo;
				header("Location: ".LEPTON_URL."/account/signup.php ");
				die();
		}
		$fields = array(
			'registered' => $timestamp,
			'login_ip' => $unix,
			'active'	=> '1',
			'statusflags' => '16',
			'password' => $password
		);
			
		$database->build_and_execute(
			'UPDATE',
			TABLE_PREFIX."users",
			$fields,
			"`hash`='".$is_user['hash']."'"
		);

		// send automatic welcome message if needed
		if(file_exists(LEPTON_PATH.'/modules/signup/welcome.html')) 
		{			
			$file_html = LEPTON_URL.'/modules/signup/welcome.html';
			$content_html = file_get_contents($file_html);

			//	1. Create a new PHPMailer instance
			$mail = LEPTON_mailer::getInstance();
			$mail->CharSet = DEFAULT_CHARSET;	

			//	2. Set who the message is to be sent from
			$mail->setFrom(SERVER_EMAIL,MAILER_DEFAULT_SENDERNAME);
			
			//	3. Set who the message is to be sent to
			$mail->addAddress($is_user['email'], $is_user['display_name']);	
			
			//Send bcc to admin
			//$mail->addBCC(SERVER_EMAIL,MAILER_DEFAULT_SENDERNAME);

			//	4. Set the subject line
			//$mail->Subject = $oCO->language['welcome_ref'];
			$mail->Subject = $oSU->language['welcome_ref'];

			//	5. Read an HTML message body from an external file, convert referenced images to embedded,
			$mail->msgHTML( $content_html );
			$mail->IsHTML(true);
			$mail->Body = $content_html;


			//	6. send the message, check for errors
			if (!$mail->send()) 
			{
					$_SESSION["signup_error"] = "Mailer Error: " . $mail->ErrorInfo;
					header("Location: ".LEPTON_URL."/account/signup.php ");
					exit();
			}
				
		}

		
		// Send info to admin and clear "old" adresses
		$mail = LEPTON_mailer::getInstance();
		
		$mail->CharSet = DEFAULT_CHARSET;	
		//Set who the message is to be sent from
		$mail->setFrom(SERVER_EMAIL,MAILER_DEFAULT_SENDERNAME);
		//Set who the message is to be sent to
		$mail->addAddress(SERVER_EMAIL,MAILER_DEFAULT_SENDERNAME);
		//Set the subject line
		$mail->Subject = $MESSAGE['SIGNUP2_ADMIN_SUBJECT'];
		//Switch to TEXT messages
		$mail->IsHTML(true);
		// Replace placeholders from language variable with values
		$values = array(
			"\n"	=> "<br />",
			'{LOGIN_NAME}'	 =>  $is_user['display_name'],
			'{LOGIN_ID}'	 =>  $is_user['user_id'],
			'{LOGIN_EMAIL}'	 =>  $is_user['email'],
			'{LOGIN_IP}'	 =>  $_SERVER['REMOTE_ADDR'],
			'{SIGNUP_DATE}'	 =>  date("Y.m.d H:i:s")	
		);
		$mail_message = str_replace( array_keys($values), array_values($values),$MESSAGE['SIGNUP2_ADMIN_INFO']);
		$mail->Body = $mail_message;		

		//send the message, check for errors
		$mail->send();	

		
		//	set up the success-message
		$_SESSION["signup_message"] = $MOD_SIGNUP['signup_confirm_info'];
		header("Location: ".LEPTON_URL."/account/signup.php ");		
		exit();
	}

} // end email verification via hash
else 
{
		$_SESSION["signup_error"] = $MOD_SIGNUP['error_info_admin'];
		header("Location: ".LEPTON_URL."/account/signup.php ");
		exit();
}	
