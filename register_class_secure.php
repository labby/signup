<?php

/**
 * @module          Signup
 * @author          cms-lab
 * @copyright      	2014-2025 CMS-LAB
 * @link            https://cms-lab.com
 * @license         GNU GPL https://www.gnu.org/licenses/gpl-3.0.en.html
 * @license_terms   none
 *
 */
 
$files_to_register = array(
	'signup.php',	
	'signup_check.php',			
	'headers.inc.php'
);

LEPTON_secure::getInstance()->accessFiles( $files_to_register );
