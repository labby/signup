<?php

/**
 * @module          Signup
 * @author          cms-lab
 * @copyright      	2014-2025 CMS-LAB
 * @link            https://cms-lab.com
 * @license         GNU GPL https://www.gnu.org/licenses/gpl-3.0.en.html
 * @license_terms   none
 *
 */

 class signup extends LEPTON_abstract
 {
	public array $users = [];
	public string $addon_color = 'blue';
	public string $cancel = ADMIN_URL.'/pages/modify.php?page_id=';
	public string $action = LEPTON_URL.'/modules/signup/';	
	
	public string $image_url = "https://cms-lab.com/_documentation/media/signup/signup.jpg";
	public string $support_link = "<a href='#'>NO Live-Support / FAQ</a>";
	public string $readme_link = "<a href='https://cms-lab.com/_documentation/signup/readme.php' target='_blank'>Readme</a>";	
	public string $readme = "https://cms-lab.com/_documentation/signup/readme.php";	

	public object|null $oTwig = null;
	public LEPTON_database $database;
	static $instance;
	
public function initialize() 
	{
		$this->database = LEPTON_database::getInstance();	
		$this->oTwig = lib_twig_box::getInstance();
		$this->oTwig->registerModule('signup');	
		$this->init_tool();		
	}	 

public function init_tool( $sToolname = '' )
	{
		if(!defined('PAGE_ID')) 
		{
			define ('PAGE_ID','0');
		}
		
		//get array of settings
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."users WHERE active = 1 AND contact_type != '' ",
			true,
			$this->users,
			true
		);		
	}
	

public function list_users()
	{
		// data for twig template engine	
		$data = array(
			'oSU'			=> $this,		
			'users'			=> $this->users,
			'leptoken'		=> get_leptoken()
			);

		// get the template-engine
		echo $this->oTwig->render( 
			"@signup/list.lte",	//	template-filename
			$data					//	template-data
		);
	}	


public function show_info() 
{
		// data for twig template engine	
		$data = array(
			'oSU'			=> $this,	
			'image_url'		=> 'https://cms-lab.com/_documentation/media/signup/signup.jpg'
			);

		// get the template-engine
		echo $this->oTwig->render( 
			"@signup/info.lte",	//	template-filename
			$data					//	template-data
		);	
	}
 }
