<?php

/**
 * @module          Signup
 * @author          cms-lab
 * @copyright      	2014-2025 CMS-LAB
 * @link            https://cms-lab.com
 * @license         GNU GPL https://www.gnu.org/licenses/gpl-3.0.en.html
 * @license_terms   none
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

$module_directory     = 'signup';
$module_name          = 'Signup';
$module_function      = 'tool';
$module_version       = '1.7.0';
$module_platform      = '7.x';
$module_author        = 'cms-lab';
$module_home          = 'https://cms-lab.com';
$module_guid          = '533a3e58-8193-4595-9bbc-92e713b48b58';
$module_license       = '<a href="https://cms-lab.com/_documentation/signup/license.php" target="_blank">GNU GPL</a>';
$module_license_terms = 'none';
$module_description   = 'Signup with frontend login and frontend register procedure';
