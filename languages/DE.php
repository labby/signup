<?php

/**
 * @module          Signup
 * @author          cms-lab
 * @copyright      	2014-2025 CMS-LAB
 * @link            https://cms-lab.com
 * @license         GNU GPL https://www.gnu.org/licenses/gpl-3.0.en.html
 * @license_terms   none
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

$MOD_SIGNUP = array(
	'LOGIN'	=> "Bitte einloggen",
	'OR'	=> "oder",
	'REGISTER'			=> "Registrieren",
	'CHOSE_CONTACT_TYPE'=> '<p>Bitte wählen Sie einen Kontakt-Typ<br />Achtung: Der Kontakt-Typ kann nach der Registrierung nicht mehr geändert werden!</p>',
	'EMAIL_WARNING'		=> '<p>Bitte geben Sie eine gültige Email-Adresse ein!</p>',
	'CONTACT_TYPE'		=> "Kontakt Typ",
	'AGREE_TERMS'		=> "Ich stimme den Bedingungen zu",
	'PERSON'			=> "Person",
	'COMPANY'			=> "Firma",
	'RETYPE_PASSWORD'	=> "Passwort wiederholen",
	'PERS_SETTINGS'		=> "Persönliche Einstellungen",
	
// messages
	'pw_message1'	=> "Bitte erzeugen sie hier direkt ein sicheres Passwort",
	'pw_message2'	=> "und tragen Sie es unten zum Schutz Ihrer persönlichen Daten ein!",
	'pw_message3'	=> "Bitte benutzen Sie nur ein sicheres Passwort (mindestens 10 Zeichen)",
	'terms'			=> "Sie müssen den Bedingungen zustimmen",
	'wrong_captcha'	=> "Bitte das korrekte Ergebnis eintragen!",
	'email'			=> "Bitte eine gültige Email-Adresse eintragen",
	'display_name'	=> "Bitte einen Namen eintragen",	
	'already_signup'=> "Sie sind bereits registriert",
	'signup_subject'=> "Ihre Registrierung",
	'signup_text'	=> "Sie haben sich registriert.<br />Zur Verifizierung Ihrer Mail-Adresse klicken Sie bitte auf folgenden Link:<br /><a href='%s'>Email Verifizierung</a>",
	'signup_info'	=> "Danke für die Registrierung. <br /> Sie werden eine Email erhalten, um Ihre Email-Adresse zu verifizieren.<br />Bitte prüfen Sie auch ihr Spamverzeichnis.",
	'already_verfied' => "Sie haben Ihre Mailadresse bereits verifiziert.",
	'confirm_text'	=> "Sie haben sich auf unserer Seite angemeldet.<br />Zur Eingabe Ihres Passwortes klicken Sie bitte auf folgenden Link:<br /><a href='%s'>Passworteingabe</a>",
	'signup_confirm_info' =>"Danke für die Verifizierung der Email-Adresse. Sie erhalten eine Mail, um Ihr Passwort einzutragen.",
	'error_info_admin'	=> "FEHLER,das sollte nicht sein.<br /> Bitte informieren Sie uns über diesen Fehler. <br /> Danke",
	'welcome_ref'	=> "Herzlich Willkommen",
);
?>