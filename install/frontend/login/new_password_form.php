<?php

/**
 * @module          Signup
 * @author          cms-lab
 * @copyright      	2014-2025 CMS-LAB
 * @link            https://cms-lab.com
 * @license         GNU GPL https://www.gnu.org/licenses/gpl-3.0.en.html
 * @license_terms   none
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


global $MESSAGE;
$database = LEPTON_database::getInstance();
// display success message from save new password
if(isset ($_SESSION["new_password_message"])) {
	echo $_SESSION["new_password_message"];
	unset($_SESSION["new_password_message"]);
	return 0;
}

// create timestamp
$current_time = time();

if(isset($_GET['hash']) && ($_GET['hash'] != "") ) {
	$confirm = intval($_GET['hash']);
} 
else 
{	
	$confirm = NULL; 
}

if(isset($_GET['signup']) && ($_GET['signup'] == "1") ) {
	$signup = true;
} 
else 
{
	$signup = false;		
}

// check if link is too old
if ($current_time > ($confirm + MAX_REGISTRATION_TIME) || ($current_time < $confirm)) 
{
	echo LEPTON_tools::display($MESSAGE['FORGOT_CONFIRM_OLD'],'pre','ui red message');	
} 
else 
{
	//	check if hash is in database
	$user = array();
	$database->execute_query(
		"SELECT * FROM `".TABLE_PREFIX."users` WHERE login_ip = '".$confirm."' ",
		true,
		$user,
		false
	);
	
	if(empty($user)) 
	{
		// info should not be displayed for security reasons
		echo LEPTON_tools::display('','pre','ui message');
	}	
	else 
	{
		// initialize twig template engine
		$oTWIG = lib_twig_box::getInstance();

		// register path to make sure twig is looking in this module template folder first
		$oTWIG->registerPath( dirname(__FILE__)."/templates/" );		
		
		// Frontend-template?
		$temp_look_for_path = LEPTON_PATH."/templates/".DEFAULT_TEMPLATE."/frontend/login/templates/";
		if(file_exists(	$temp_look_for_path."new_password_form.lte")) {
			$oTWIG->registerPath( $temp_look_for_path );
		}
			
		 //	Delete any "result_message" if there is one.
		if( true === isset($_SESSION['result_message']) )
		{
			unset($_SESSION['result_message']);	
		}
		
		// check if pw_generator is installed
		$check_addon = $database->get_one("SELECT addon_id FROM ".TABLE_PREFIX."addons WHERE directory = 'pw_generator'");
		if($check_addon === NULL) {
			$pw_generator = 0;
		} 
		else 
		{
			$pw_generator = 1;
		}
						
		$data = array(
			'MOD_SIGNUP'				=> signup::getInstance(),
			'pw_generator'				=> $pw_generator,
			'RESULT_MESSAGE'			=> (isset($_SESSION['result_message'])) ? $_SESSION['result_message'] : "",
			'NEW_PASSWORD_URL'			=>	LEPTON_URL.'/account/save_new_password.php',				
			'TEMPLATE_DIR' 				=>	TEMPLATE_DIR,
			'hash'						=>	$confirm,
			'r_time'					=>	$current_time,
			'signup'					=>	((true === $signup) ? 1 : 0),	// make sure 'signup' has a valid integer			
			//	Min- length of passwords
			'AUTH_MAX_LOGIN_LENGTH'		=>	AUTH_MAX_LOGIN_LENGTH,			
			'AUTH_MAX_PASS_LENGTH'		=>	AUTH_MAX_PASS_LENGTH,	
			'AUTH_MIN_PASS_LENGTH'		=>	AUTH_MIN_PASS_LENGTH					
		);
		
		echo $oTWIG->render("new_password_form.lte",$data);				
	}	
}