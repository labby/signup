<?php

/**
 * @module          Signup
 * @author          cms-lab
 * @copyright      	2014-2025 CMS-LAB
 * @link            https://cms-lab.com
 * @license         GNU GPL https://www.gnu.org/licenses/gpl-3.0.en.html
 * @license_terms   none
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

	

/**	*********
 *	languages
 *
 */
$languages = array();
$database->execute_query( 
	"SELECT `directory`,`name` from `".TABLE_PREFIX."addons` where `type`='language'",
 	true,
 	$languages,
 	true
);


$language = array();
foreach($languages as $data)
{
	$language[] = array(
		'LANG_CODE' 	=>	$data['directory'],
		'LANG_NAME'		=>	$data['name'],
		'LANG_SELECTED'	=> (LANGUAGE == $data['directory']) ? " selected='selected'" : ""
	);
}


/**	****************
 *	default timezone
 *
 */
$timezone_table = LEPTON_date::get_timezones();
$timezone = array();
$current_timezone_string = $oLEPTON->get_timezone_string();
foreach ($timezone_table as $title)
{
	$timezone[] = array(
		'TIMEZONE_NAME' => $title,
		'TIMEZONE_SELECTED' => ($current_timezone_string == $title) ? ' selected="selected"' : ''
	);
}

/**	***********
 *	date format
 */
$date_format = array();
$user_time = true;

$DATE_FORMATS = LEPTON_date::get_dateformats();

foreach($DATE_FORMATS AS $format => $title) 
{	
	if($format == "system_default")
	{
		continue;	
	}
	
	if(DATE_FORMAT == $format && !isset($_SESSION['USE_DEFAULT_DATE_FORMAT'])) 
	{
		$sel = "selected='selected'";
	}
	elseif($format == 'system_default' && isset($_SESSION['USE_DEFAULT_DATE_FORMAT'])) 
	{
		$sel = "selected='selected'";
	} 
	else 
	{
		$sel = '';	
	}
	
	$date_format[] = array(
		'DATE_FORMAT_VALUE'	=>	$format,
		'DATE_FORMAT_TITLE'	=>	$title.( $format === DEFAULT_DATE_FORMAT ? " (system default)" : "" ),
		'DATE_FORMAT_SELECTED' => $sel
	);

}

/**	***********
 *	time format
 */
$time_format = array();

$TIME_FORMATS = LEPTON_date::get_timeformats();
foreach($TIME_FORMATS AS $format => $title) {

	if($format == 'system_default') 
	{
		continue;
	}

	if(TIME_FORMAT == $format && !isset($_SESSION['USE_DEFAULT_TIME_FORMAT'])) 
	{
		$sel = "selected='selected'";	
	} 
	elseif($format == 'system_default' && isset($_SESSION['USE_DEFAULT_TIME_FORMAT'])) 
	{
		$sel = "selected='selected'";
	} 
	else 
	{
		$sel = '';
	}			
	
	$time_format[] = array(
		'TIME_FORMAT_VALUE'	=>	$format,
		'TIME_FORMAT_TITLE'	=>	$title.( $format === DEFAULT_TIME_FORMAT ? " (system default)" : "" ),
		'TIME_FORMAT_SELECTED' => $sel
	);
}

//	Build an access-preferences-form secure hash
LEPTON_handle::register("random_string");
$hash = sha1( microtime().$_SERVER['HTTP_USER_AGENT'].random_string(32) );
$_SESSION['wb_apf_hash'] = $hash;

/**
 *	Delete any "result_message" if there is one.
 */
if( true === isset($_SESSION['result_message']) )
{
	unset($_SESSION['result_message']);	
}

$data = array(
	'TEMPLATE_DIR' 				=>	TEMPLATE_DIR,
	'PREFERENCES_URL'			=>	PREFERENCES_URL,
	'LOGOUT_URL'				=>	LOGOUT_URL,
	'DISPLAY_NAME'				=>	$oLEPTON->get_display_name(),
	'GET_EMAIL'					=>	$oLEPTON->get_email(),
	'USER_ID'					=>	(isset($_SESSION['USER_ID']) ? $_SESSION['USER_ID'] : '-1'),
	'r_time'					=>	time(),
	'HASH'						=>	$hash,
	'RESULT_MESSAGE'			=> (isset($_SESSION['result_message'])) ? $_SESSION['result_message'] : "",
	//	Min- length of passwords			
	'AUTH_MAX_PASS_LENGTH'		=>	AUTH_MAX_PASS_LENGTH,	
	'AUTH_MIN_PASS_LENGTH'		=>	AUTH_MIN_PASS_LENGTH,	
	'AUTH_MIN_LOGIN_LENGTH'		=>	AUTH_MIN_LOGIN_LENGTH,
	'AUTH_MAX_LOGIN_LENGTH'		=>	AUTH_MAX_LOGIN_LENGTH,
	'language'	=> $language,
	'timezone'	=> $timezone,
	'date_format' => $date_format,
	'time_format' => $time_format
	
);

$oTWIG = lib_twig_box::getInstance();
$oTWIG->registerPath( dirname(__FILE__)."/templates/" );
// "login" got a slide another frontend path ... as THERE IS NO MODULE "login" we have to look ourself!
$oTWIG->registerPath( LEPTON_PATH."/templates/".DEFAULT_TEMPLATE."/frontend/login/templates/" );

echo $oTWIG->render("preferences_form.lte", $data);	