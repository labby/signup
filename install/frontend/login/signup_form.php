<?php

/**
 * @module          Signup
 * @author          cms-lab
 * @copyright      	2014-2025 CMS-LAB
 * @link            https://cms-lab.com
 * @license         GNU GPL https://www.gnu.org/licenses/gpl-3.0.en.html
 * @license_terms   none
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

//verify all data
if (isset($_GET['hash']) ) 
{
	require_once LEPTON_PATH."/modules/signup/signup_check.php";
}

// get the template-engine and register path to make sure twig is looking in this module template folder first
$oTWIG = lib_twig_box::getInstance();
$oTWIG->registerPath( LEPTON_PATH."/templates/".DEFAULT_TEMPLATE."/frontend/login/templates/", 'signup' );
global $section_id;
	
//	Get the captha
ob_start();
captcha_control::getInstance()->call_captcha("all","",(int)$section_id);
$captcha = ob_get_clean();
	
// create timestamp for multiple use
$timestamp = date('Y-m-d H:i:s',time());
$unix = time();

$_SESSION['submitted_when'] = $unix;

$data = array(
	'oSU' 			=>  signup::getInstance()->language,
	'SIGNUP_URL'	=>	LEPTON_URL."/modules/signup/signup.php",
	'CALL_CAPTCHA'	=>	$captcha,     
	'AUTH_MAX_LOGIN_LENGTH'	=>	AUTH_MAX_LOGIN_LENGTH,	
	'signup_message'=> (isset($_SESSION["signup_message"]) ? $_SESSION["signup_message"] : ''),
	'signup_error'	=> (isset($_SESSION["signup_error"]) ? $_SESSION["signup_error"] : ''),
	'submitted_when'=> $unix
	);
		
echo $oTWIG->render( 
	"@signup/signup_form.lte",	//	template-filename
	$data				//	template-data
);

if (isset($_SESSION["signup_message"])) 
{
	unset ($_SESSION["signup_message"]);
}
if (isset($_SESSION["signup_error"]))
{
	unset ($_SESSION["signup_error"]);
}