<?php

/**
 * @module          Signup
 * @author          cms-lab
 * @copyright      	2014-2025 CMS-LAB
 * @link            https://cms-lab.com
 * @license         GNU GPL https://www.gnu.org/licenses/gpl-3.0.en.html
 * @license_terms   none
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

/* Include template engine */
$oTWIG = lib_twig_box::getInstance();

// register path to make sure twig is looking in this module template folder first
$oTWIG->registerPath( dirname(__FILE__)."/templates/" );

//Building a secure-hash
$hash = sha1( microtime().$_SERVER['HTTP_USER_AGENT'] );
$_SESSION['wb_apf_hash'] = $hash;

$thisApp = LEPTON_login::getInstance();
unset($_SESSION['result_message']);


$salt = md5(microtime());
/**
 *	we want different hashes for the two fields
 *
 */
$username_fieldname = 'username_'.substr($salt, 0, 7);
$password_fieldname = 'password_'.substr($salt, -7);

if(TFA === true){
	$login_url = LEPTON_URL.'/account/login.php' ;
} 
else 
{
	$login_url = LOGIN_URL;
}

$data = array(
	'oSU'			=>	signup::getInstance()->language,
	'LOGIN_URL'		=>	LOGIN_URL,
	'SIGNUP_URL'	=>	SIGNUP_URL,	
	'LOGOUT_URL'	=>	LOGOUT_URL,
	'FORGOT_URL'	=>	FORGOT_URL,  
	'MESSAGE'		=>	$thisApp->message, 
	'signup_message'=> (isset($_SESSION["signup_message"]) ? $_SESSION["signup_message"] : ''),	
	'REDIRECT_URL'	=>	$thisApp->redirect_url,   
	'HASH'			=>	$hash,
	'username_fieldname'    => $username_fieldname,
	'password_fieldname'    => $password_fieldname
);
	
echo $oTWIG->render("login_form.lte", $data);
	
if (isset($_SESSION["signup_message"])) 
{
	unset ($_SESSION["signup_message"]);	
}
if (isset($_SESSION["result_message"]))
{
	unset ($_SESSION["result_message"]);	
}
