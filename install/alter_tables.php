<?php

/**
 * @module          Signup
 * @author          cms-lab
 * @copyright      	2014-2025 CMS-LAB
 * @link            https://cms-lab.com
 * @license         GNU GPL https://www.gnu.org/licenses/gpl-3.0.en.html
 * @license_terms   none
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

	
$database = LEPTON_database::getInstance();
$admin = LEPTON_admin::getInstance();

// rename standard login
$directory_names = array(
	array ('source' => "/templates/".DEFAULT_TEMPLATE."/frontend/login", 'target'=> "/templates/".DEFAULT_TEMPLATE."/frontend/login_standard")
);
LEPTON_handle::rename_directories($directory_names);

// move new module files to default template
$directory_names = array(
	array ('source' => "/modules/signup/install/frontend/login", 'target'=> "/templates/".DEFAULT_TEMPLATE."/frontend/login")
);
LEPTON_handle::rename_directories($directory_names);


// extend standard user table
$table = TABLE_PREFIX .'users'; 
$database->simple_query("ALTER TABLE `".$table."`  ADD `contact_type` VARCHAR(32) NOT NULL DEFAULT '' ");
$database->simple_query("ALTER TABLE `".$table."`  ADD `registered` timestamp NULL DEFAULT NULL ");
$database->simple_query("ALTER TABLE `".$table."`  ADD `hash` VARCHAR(64) NOT NULL DEFAULT '' ");	
$database->simple_query("ALTER TABLE `".$table."`  ADD `unix_time` VARCHAR(14) NOT NULL DEFAULT '' ");
	
// check if group is in database
$signup_group = array();
$database->execute_query(
	"SELECT * FROM ".TABLE_PREFIX."groups WHERE `name` = 'Auto-Signup'  ",
	TRUE,
	$signup_group, 
	FALSE
);
if (empty($signup_group)) {
//	create new user group
	$database->simple_query("INSERT INTO `".TABLE_PREFIX ."groups` (`group_id`, `name`, `system_permissions`, `module_permissions`, `template_permissions`, `language_permissions`) VALUES
	(NULL, 'Auto-Signup', 'pages,pages_view','','','')
	");
}

//	modify settings table
$group_id = $database->get_one("SELECT LAST_INSERT_ID()");

$database->simple_query("UPDATE `" . TABLE_PREFIX ."settings` SET `value` ='".$group_id."' WHERE `name` ='frontend_signup'");
$database->simple_query("UPDATE `" . TABLE_PREFIX ."settings` SET `value` ='true' WHERE `name` ='frontend_login'");
