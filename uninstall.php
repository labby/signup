<?php

/**
 * @module          Signup
 * @author          cms-lab
 * @copyright      	2014-2025 CMS-LAB
 * @link            https://cms-lab.com
 * @license         GNU GPL https://www.gnu.org/licenses/gpl-3.0.en.html
 * @license_terms   none
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

// delete signup/login installed templates
LEPTON_handle::register ("rm_full_dir");
if(file_exists(LEPTON_PATH."/templates/".DEFAULT_TEMPLATE."/frontend/login/index.php")) 
{
	rm_full_dir(LEPTON_PATH."/templates/".DEFAULT_TEMPLATE."/frontend/login");
}

// see if current theme frontend_login was modified
LEPTON_handle::register ("rename_recursive_dirs");
$directory = LEPTON_PATH."/templates/".DEFAULT_TEMPLATE."/frontend/login_standard";
$directory_new = LEPTON_PATH."/templates/".DEFAULT_TEMPLATE."/frontend/login";
if(file_exists(LEPTON_PATH."/templates/".DEFAULT_TEMPLATE."/frontend/login_standard/index.php"))
{
	rename_recursive_dirs( $directory,$directory_new);
}

// alter standard user table
$table = TABLE_PREFIX .'users'; 
$database->simple_query("ALTER TABLE `".$table."`  DROP COLUMN `contact_type` ");
$database->simple_query("ALTER TABLE `".$table."`  DROP COLUMN `registered` ");
$database->simple_query("ALTER TABLE `".$table."`  DROP COLUMN `hash` ");	
$database->simple_query("ALTER TABLE `".$table."`  DROP COLUMN `unix_time` ");	
